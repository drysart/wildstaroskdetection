﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication2
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    internal struct RGBA
    {
        public byte R;
        public byte G;
        public byte B;
        public byte A;
    }
}
