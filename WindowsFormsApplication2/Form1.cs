﻿using ConsoleApplication4;
using SlimDX.Direct3D9;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication2.Win32Interop;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //System.Diagnostics.Process.GetCurrentProcess().PriorityClass =
            //     ProcessPriorityClass.BelowNormal;
        }

        /// <summary>
        /// Process a screenshotted frame from the WildStar client
        /// </summary>
        /// <param name="i">The bitmap of the screenshot</param>
        /// <param name="hWnd">The Win32 handle to the WildStar window</param>
        /// <returns>See DoIt() for why this returns a Task</returns>
        private async Task ProcessBitmap(IBitmap i, IntPtr hWnd)
        {
            Stopwatch sw = Stopwatch.StartNew();

            // Step 1: See if the OSK is being shown.
            Point digitsLoc;
            var bmps = ScanBitmap(i, out digitsLoc);
            if (bmps != null)
            {
                // The OSK is being shown!
                // Step 2: Figure out what digits are on the OSK's buttons.
                int[,] digits = new int[2, 5];
                StringBuilder sb = new StringBuilder(10);
                for (var y = 0; y < 2; ++y)
                {
                    for (var x = 0; x < 5; ++x)
                    {
                        int digit = RecognizeDigit(bmps[y, x]);
                        digits[y, x] = digit;
                        bmps[y, x].Dispose();
                    }
                }

                // Step 3: Sanity check the OSK buttons as detected.
                if (hWnd != IntPtr.Zero && digitsLoc != Point.Empty && VerifyAllDigits(digits))
                {
                    // Ok so this code here is left in a state of mid-change.  It used to automatically
                    // generate and type in your OTP.  Now it hooks the keyboard and clicks the buttons
                    // for you as you type in the digits and hit Enter.
                    // The code is a bit of a mess because of these un-refactored changes. Sorry about that.

                    this.OtpDetect = digits;
                    // The OTP Latch was for when this would type in your OTP automatically.  After it clicked
                    // in your code, the OSK would remain on the screen for a few seconds while your login
                    // was sent to Carbine and validated.  The latch prevents the code from immediately re-detecting
                    // the still-on-screen-OSK and trying to type your code into it again.  Basically, after it
                    // goes into EnteringCode state, it's latched until it successfully sees a screen that
                    // *doesn't* have the OSK on it -- and while it's latched it won't try to type in your OTP
                    // again.  Since the code was updated to not type in the code automatically and instead
                    // translate keyboard entry into clicks, the latch now does nothing.
                    if (this.OtpLatch == OtpLatchState.None ||
                        this.OtpLatch == OtpLatchState.Invalid)
                    {
                        this.OtpLatch = OtpLatchState.EnteringCode;

                        m_enterKeyProc = (k) =>
                            {
                                // Handle keystrokes and turn relevant keystrokes into clicks on the OSK.

                                POINT p = new POINT(0, 0);
                                NativeMethods.ClientToScreen(hWnd, ref p);

                                string kDigit;
                                if ((int)k >= (int)KeyboardHookKey.K0 &&
                                    (int)k <= (int)KeyboardHookKey.K9)
                                {
                                    kDigit = ((int)k).ToString();

                                    EnterAuthCode(hWnd,
                                        digits,
                                        new Point(digitsLoc.X + p.X, digitsLoc.Y + p.Y),
                                        kDigit);
                                }
                                else if (k == KeyboardHookKey.Backspace)
                                {
                                    // 193, 117
                                    SendClick(hWnd,
                                        new Point(digitsLoc.X + 196, digitsLoc.Y + 113));
                                }
                                else if (k == KeyboardHookKey.Escape)
                                {
                                    // 273, -78
                                    SendClick(hWnd,
                                        new Point(digitsLoc.X + 273, digitsLoc.Y - 78));
                                }
                                else if (k == KeyboardHookKey.Enter)
                                {
                                    // 89, 186
                                    SendClick(hWnd, new Point(digitsLoc.X + 89, digitsLoc.Y + 186));
                                }


                                //string authCode = GenerateAuthenticatorCode();
                                //await EnterAuthCode(hWnd, digits,
                                //    new Point(digitsLoc.X + p.X, digitsLoc.Y + p.Y),
                                //    authCode);
                            };
                        //this.OtpLatch = OtpLatchState.WaitingForCodeProcessing;
                    }
                }
                else
                {
                    this.OtpDetect = null;
                }
            }
            else
            {
                this.OtpDetect = null;
            }

            sw.Stop();
        }

        private Action<KeyboardHookKey> m_enterKeyProc = null;
        private bool m_needToQueueKeys = false;
        private readonly Queue<Action> m_queuedKeys = new Queue<Action>();

        private async Task EnterAuthCode(IntPtr hWnd, int[,] digits, Point digitsLoc, string authCode)
        {
            foreach (var ch in authCode)
            {
                var chDigit = Convert.ToInt32(ch.ToString());
                var dp = FindDigitPoint(digits, digitsLoc, chDigit);
                await SendClick(hWnd, dp);
            }

            // This commented click was to click on the submit button after entering the OTP automatically.
            //await SendClick(hWnd, new Point(digitsLoc.X + 89, digitsLoc.Y + 186));
        }

        private async Task SendClick(IntPtr hWnd, Point p)
        {
            // Sending a click consists of three operations with a short delay between each:
            //  * Move the mouse to where you want to click
            //  * Put the left mouse button down
            //  * Lift the left mouse button up

            // This queue is part of the change to translate keyboard keystrokes into clicks... we don't
            // want to overlap movements if the user hits the keystrokes too fast, so we queue up
            // additional click requests to run after the current SendClick is done.
            if (m_needToQueueKeys)
            {
                m_queuedKeys.Enqueue(() => SendClick(hWnd, p));
                return;
            }

            m_needToQueueKeys = true;
            //if (m_kbHook != null)
            //    m_kbHook.Paused = true;
            NativeMethods.SetCursorPos(p.X, p.Y);
            await Pause(TimeSpan.FromSeconds(0.02));
            NativeMethods.mouse_event(MouseEventFlags.LEFTDOWN, (uint)p.X, (uint)p.Y, MouseEventDataXButtons.None, UIntPtr.Zero);
            await Pause(TimeSpan.FromSeconds(0.02));
            NativeMethods.mouse_event(MouseEventFlags.LEFTUP, (uint)p.X, (uint)p.Y, MouseEventDataXButtons.None, UIntPtr.Zero);
            await Pause(TimeSpan.FromSeconds(0.02));
            //if (m_kbHook != null)
            //    m_kbHook.Paused = false;
            if (m_queuedKeys.Count > 0)
            {
                m_needToQueueKeys = false;
                m_queuedKeys.Dequeue()();
            }
            else
            {
                m_needToQueueKeys = false;
            }
        }

        private System.Windows.Forms.Timer m_pauseTimer;
        private SortedList<DateTime, Action> m_pauseQueue = new SortedList<DateTime, Action>();

        private Task Pause(TimeSpan span)
        {
            // Yes I know this method is a huge hack and there are better ways to do this.

            TaskCompletionSource<object> tcs = new TaskCompletionSource<object>();

            if (m_pauseTimer == null)
            {
                m_pauseTimer = new System.Windows.Forms.Timer();
                m_pauseTimer.Tick += (s, e) =>
                    {
                        while (m_pauseQueue.Count > 0)
                        {
                            var next = m_pauseQueue.First();
                            if (next.Key < DateTime.Now)
                            {
                                next.Value();
                                m_pauseQueue.Remove(next.Key);
                            }
                            else
                            {
                                break;
                            }
                        }

                        UpdatePauseTimerTick();
                    };
            }

            m_pauseQueue.Add(DateTime.Now + span, () =>
                {
                    tcs.SetResult(null);
                });

            UpdatePauseTimerTick();
            return tcs.Task;
        }
        private void UpdatePauseTimerTick()
        {
            if (m_pauseQueue.Count > 0)
            {
                var tickAt = m_pauseQueue.First().Key;
                var tickSpan = (int)Math.Round(Math.Abs((tickAt - DateTime.Now).TotalMilliseconds));
                if (tickSpan <= 0)
                    tickSpan = 1;
                m_pauseTimer.Interval = tickSpan;
                m_pauseTimer.Enabled = true;
            }
        }

        private Point FindDigitPoint(int[,] digits, Point digitsLoc, int tdigit)
        {
            for (int y = 0; y < 2; y++)
            {
                for (int x = 0; x < 5; x++)
                {
                    if (digits[y, x] == tdigit)
                    {
                        return new Point(digitsLoc.X + (55 * x), digitsLoc.Y + (55 * y));
                    }
                }
            }
            return Point.Empty;
        }

        const string KeyString = "**AUTHENTICATOR SERIAL CODE GOES HERE**";

        /// <summary>
        /// Generates the 6-digit authenticator code.  If you want to productionize this, you should
        /// make sure the user's clock is correct.  WinAuth hits google.com and works off of the timestamp
        /// that gets returned via its HTTP response headers.
        /// </summary>
        /// <returns>The code.</returns>
        private string GenerateAuthenticatorCode()
        {
            byte[] k = Base32.FromBase32String(KeyString);
            return TOTP.GenerateTOTP(k, DateTime.Now, 6);
        }

        /// <summary>
        /// Quick check to make sure that each digit 0 through 9 is represented; and that
        /// each digit only appears once.
        /// </summary>
        /// <param name="digits">An int[2,5] of the digits on the OSK buttons</param>
        /// <returns>True if the digits are ok, false if not.</returns>
        private bool VerifyAllDigits(int[,] digits)
        {
            bool[] seenDigit = new bool[10];
            for (int y = 0; y < 2; ++y)
            {
                for (int x = 0; x < 5; ++x)
                {
                    int digit = digits[y, x];
                    if (seenDigit[digit])
                        return false;
                    else
                        seenDigit[digit] = true;
                }
            }
            return true;
        }

        private Dictionary<int, byte[]> m_digitRecognitionData;

        private Stream GetManifestStreamByPartialName(string partialName)
        {
            partialName = "." + partialName;
            foreach (var thisStreamName in System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceNames())
            {
                if (thisStreamName.EndsWith(partialName, StringComparison.OrdinalIgnoreCase))
                {
                    return System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(thisStreamName);
                }
            }
            throw new Exception("invalid name");
        }

        private unsafe void PopulateDigitRecognitionData()
        {
            // There are 10 resource bitmaps included in this project, one for each digit 0 through 9.  We pull them
            // in here and save their Green channel for later comparisons.  (The green channel is the channel with the
            // most contrast, and thus best for comparing.)

            var dict = new Dictionary<int, byte[]>();

            for (int i = 0; i < 10; ++i)
            {
                using (var refBmp = (Bitmap)Image.FromStream(GetManifestStreamByPartialName(String.Format("ws-{0}.png", i))))
                {
                    var bHeight = refBmp.Height;
                    var bWidth = refBmp.Width;
                    byte[] tbuf = new byte[bHeight * bWidth];
                    BitmapData refBd = refBmp.LockBits(new Rectangle(0, 0, refBmp.Width, refBmp.Height), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
                    try
                    {
                        byte* ref0 = (byte*)refBd.Scan0;

                        int curIdx = 0;
                        for (int y = 0; y < bHeight; ++y)
                        {
                            byte* ref_s0 = ref0 + (refBd.Stride * y);

                            RGBA* refCurPx = (RGBA*)ref_s0;

                            for (int x = 0; x < bWidth; ++x)
                            {
                                tbuf[curIdx++] = refCurPx->G;
                                refCurPx++;
                            }
                        }
                    }
                    finally
                    {
                        refBmp.UnlockBits(refBd);
                    }
                    dict.Add(i, tbuf);
                }
            }

            m_digitRecognitionData = dict;
        }

        /// <summary>
        /// Given a bitmap, which digit is shown?
        /// </summary>
        /// <param name="dbmp">The bitmap to check</param>
        /// <returns>Which digit is shown (best match)</returns>
        private unsafe int RecognizeDigit(IBitmap dbmp)
        {
            // This is a pretty naive check.  It's not "advanced OCR" by any stretch of the imagination.
            // The WildStar client always paints the digits in the same way, so we take the digits from the
            // screenshot and compare them against reference bitmaps.  Accumulate how much each pixel from the
            // screenshot differs from each pixel in the reference bitmap, and return the digit whose reference
            // bitmap accumulated the lowest difference.
            //
            // Comparisons are only done on the green channel.

            int bestOffset = 9999999;
            int bestDigit = -1;

            int bWidth = dbmp.Width;
            int bHeight = dbmp.Height;

            if (m_digitRecognitionData == null)
                PopulateDigitRecognitionData();

            byte* bd0 = dbmp.Scan0;
            // For each of the 10 reference images...
            for (int i = 0; i < 10; ++i)
            {
                fixed (byte* refBuf = m_digitRecognitionData[i])
                {
                    // Loop over all the pixels...
                    int thisOffset = 0;
                    byte* thisRefG = refBuf;

                    for (int y = 0; y < bHeight; ++y)
                    {
                        byte* bd_s0 = bd0 + (dbmp.Stride * y);

                        BGRA* bdCurPx = (BGRA*)bd_s0;

                        for (int x = 0; x < bWidth; ++x)
                        {
                            // ...and accumulate the difference.
                            thisOffset += Math.Abs(bdCurPx->G - *thisRefG);
                            // If the difference against this reference digit so far is already higher than our
                            // best match, exit this reference digit's check early.
                            if (thisOffset > bestOffset)
                            {
                                goto ENDIMAGE;
                            }
                            bdCurPx++;
                            thisRefG++;
                        }
                    }

                    // If this reference digit had less of a difference than our previous best, remember that
                    // this is our new best and how much difference we found.
                    if (thisOffset < bestOffset)
                    {
                        bestOffset = thisOffset;
                        bestDigit = i;
                    }
                ENDIMAGE:
                    thisOffset = 0;

                }
            }

            // Return the best match's digit.
            return bestDigit;
        }

        private unsafe int RecognizeDigit(Bitmap dbmp)
        {
            // This method is not used.  It's from an older version of the prototype.

            int bestOffset = 9999999;
            int bestDigit = -1;

            int bWidth = dbmp.Width;
            int bHeight = dbmp.Height;

            if (m_digitRecognitionData == null)
                PopulateDigitRecognitionData();

            BitmapData bd = dbmp.LockBits(new Rectangle(0, 0, dbmp.Width, dbmp.Height), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            try
            {
                byte* bd0 = (byte*)bd.Scan0;

                for (int i = 0; i < 10; ++i)
                {
                    fixed (byte* refBuf = m_digitRecognitionData[i])
                    {


                            int thisOffset = 0;
                            byte* thisRefG = refBuf;

                            for (int y = 0; y < bHeight; ++y)
                            {
                                byte* bd_s0 = bd0 + (bd.Stride * y);

                                RGBA* bdCurPx = (RGBA*)bd_s0;

                                for (int x = 0; x < bWidth; ++x)
                                {
                                    thisOffset += Math.Abs(bdCurPx->G - *thisRefG);
                                    if (thisOffset > bestOffset)
                                    {
                                        goto ENDIMAGE;
                                    }
                                    bdCurPx++;
                                    thisRefG++;
                                }
                            }

                            if (thisOffset < bestOffset)
                            {
                                bestOffset = thisOffset;
                                bestDigit = i;
                            }
                        ENDIMAGE:
                            thisOffset = 0;

                    }
                }
            }
            finally
            {
                dbmp.UnlockBits(bd);
            }

            return bestDigit;
        }

        /// <summary>
        /// Gets the pointer to a single pixel from an IBitmap.  This method is only used when checking pixels in
        /// a bitmap in non-scanline order.  Running through a bitmap in scanline order is greatly
        /// preferred because it's more efficient.
        /// </summary>
        /// <param name="i">The bitmap</param>
        /// <param name="x">Pixel X coordinate</param>
        /// <param name="y">Pixel Y coordinate</param>
        /// <returns>A pointer to the pixel requested</returns>
        private unsafe RGBA* GetPixel(IBitmap i, int x, int y)
        {
            byte* s0 = i.Scan0;
            var l0 = s0 + (y * i.Stride);
            var curPx = (RGBA*)l0;
            curPx += x;
            return curPx;
        }

        /// <summary>
        /// This method examines a Bitmap to see if it can be recognized as the WildStar login
        /// screen.  If so, it extracts the sub-bitmaps corresponding to each of the ten
        /// OSK buttons, and a point that points to the top left button.
        /// </summary>
        /// <param name="i">The bitmap to check</param>
        /// <param name="digitsLoc">The point of the top left OSK button, if found</param>
        /// <returns>An array of IBitmap[2,5] of the digits on each OSK button</returns>
        private unsafe IBitmap[,] ScanBitmap(IBitmap i, out Point digitsLoc)
        {
            var bmpHeight = i.Height;
            var bmpWidth = i.Width;
            var inARow = 0;

            int? digitX = null;
            int? digitY = null;

            // Step 1: look for the red (X) button that's in the top right corner of the OSK
            // prompt window.  It should be the only thing nearby that has a green channel of 0,
            // and it should have a radius of 17 pixels.
            try
            {
                byte* s0 = i.Scan0;
                for (var y = 0; y < bmpHeight; ++y)
                {
                    var l0 = s0 + (y * i.Stride);
                    var curPx = (RGBA*)l0;
                    for (var x = 0; x < bmpWidth; ++x)
                    {
                        if (curPx->G == 0)
                        {
                            inARow++;
                        }
                        else
                        {
                            inARow = 0;
                        }

                        // If we've scanned through 9 pixels in a row that have a green channel value of 0,
                        // then we might be on the center pixel of the (X) button right now.
                        if (inARow == 9)
                        {
                            // So let's check 8 and 9 pixels in each direction and make sure the 8th pixel
                            // in each direction has Green 0, and the 9th pixel in each direction has Green > 0
                            if (GetPixel(i, x, y - 8)->G == 0 &&
                                GetPixel(i, x, y - 9)->G > 0 &&
                                GetPixel(i, x, y + 8)->G == 0 &&
                                GetPixel(i, x, y + 9)->G > 0 &&
                                GetPixel(i, x + 8, y)->G == 0 &&
                                GetPixel(i, x + 9, y)->G > 0 &&
                                GetPixel(i, x - 8, y)->G == 0 &&
                                GetPixel(i, x - 9, y)->G > 0)
                            {
                                // Found the (X).  The top left button of the OSK is a known pixel offset
                                // from the center of this button.  Remember it, and jump out of the bitmap
                                // scan loops.   This point is to the top left pixel of that button's digit.
                                digitX = x - 272;
                                digitY = y + 78;
                                goto EXITLOOP;
                            }
                        }

                        curPx++;
                    }
                }
            EXITLOOP:
                inARow = 0;

            }
            finally
            {
            }

            // If we found the OSK...
            if (digitX != null)
            {
                // Lets extract a sub-bitmap of the number being displayed on each of the ten buttons
                IBitmap[,] bmps = new IBitmap[2,5];
                for (var dy = 0; dy < 2; ++dy)
                {
                    // Each button is 55 pixels apart vertically
                    var thisDigitY = digitY.Value + (dy * 55);
                    var thisDigitX = digitX.Value;
                    for (var dx = 0; dx < 5; ++dx)
                    {
                        // The digit is 11px by 14px in size
                        var dbmp = i.CreateIBitmap(new Rectangle(thisDigitX, thisDigitY, 11, 14));
                        bmps[dy, dx] = dbmp;

                        // Each button if 55 pixels apart horizontally
                        thisDigitX += 55;
                    }
                }
                // Return the point of the top left button, and all the sub-bitmaps.
                digitsLoc = new Point(digitX.Value, digitY.Value);
                return bmps;
            }
            // Didn't find the OSK, return nothing.
            digitsLoc = Point.Empty;
            return null;
        }

        private int? m_appStatus = -1;
        private int? AppStatus
        {
            get { return m_appStatus; }
            set
            {
                if (value != m_appStatus)
                {
                    m_appStatus = value;
                    UpdateStatus();
                    if (m_appStatus == null)
                        this.WindowStatus = IntPtr.Zero;
                }
            }
        }

        private IntPtr m_appWindow = new IntPtr(-1);

        private IntPtr WindowStatus
        {
            get { return m_appWindow; }
            set
            {
                if (value != m_appWindow)
                {
                    m_appWindow = value;
                    UpdateStatus();
                    if (m_appWindow == IntPtr.Zero)
                    {
                        this.LoginUiDetect = false;
                    }
                }
            }
        }

        private bool m_loginUiDetect = true;

        private bool LoginUiDetect
        {
            get { return m_loginUiDetect; }
            set
            {
                if (value != m_loginUiDetect)
                {
                    m_loginUiDetect = value;
                    UpdateStatus();
                    

                    if (!m_loginUiDetect)
                        this.OtpDetect = null;
                }
            }
        }

        private int[,] m_otpDetect = new int[2, 5];
        private KeyboardHook m_kbHook = new KeyboardHook();

        private int[,] OtpDetect
        {
            get { return m_otpDetect; }
            set
            {
                if (value != m_otpDetect)
                {
                    m_otpDetect = value;
                    UpdateStatus();
                    if (m_otpDetect == null)
                        this.OtpLatch = OtpLatchState.None;
                }
            }
        }

        void m_kbHook_KeyPressed(object sender, KeyboardHookEventArgs e)
        {
            this.BeginInvoke((MethodInvoker)delegate
            {
                if (this.OtpDetect != null && this.OtpLatch == OtpLatchState.EnteringCode && m_enterKeyProc != null)
                {
                    m_enterKeyProc(e.Key);
                    //System.Media.SystemSounds.Beep.Play();
                }
            });
        }

        private enum OtpLatchState
        {
            Invalid,
            None,
            EnteringCode,
            WaitingForCodeProcessing
        }

        private OtpLatchState m_otpLatch = OtpLatchState.Invalid;

        private OtpLatchState OtpLatch
        {
            get { return m_otpLatch; }
            set
            {
                if (value != m_otpLatch)
                {
                    m_otpLatch = value;
                    UpdateStatus();
                }
            }
        }

        private void UpdateStatus()
        {
            lblAppStatus.Text = m_appStatus != null ? "Found PID " + m_appStatus.Value.ToString() : "Not Found";

            if (m_appWindow == IntPtr.Zero)
            {
                lblWindowDetect.Text = "Not Found";
                this.LoginUiDetect = false;
            }
            else
            {
                lblWindowDetect.Text = String.Format("Found HWND {0}", m_appWindow);
            }

            lblLoginDetect.Text = m_loginUiDetect ? "Found" : "Not Found";

            if (m_otpDetect != null)
            {
                StringBuilder sb = new StringBuilder(10);
                for (int y = 0; y < 2; ++y)
                    for (int x = 0; x < 5; ++x)
                        sb.Append(m_otpDetect[y, x].ToString());
                lblOtpKeyboardDetect.Text = sb.ToString();
            }
            else
            {
                lblOtpKeyboardDetect.Text = "Not Found";
            }

            switch (m_otpLatch)
            {
                default:
                case OtpLatchState.Invalid:
                    lblOtpLatch.Text = "N/A";
                    break;
                case OtpLatchState.None:
                    lblOtpLatch.Text = "None";
                    break;
                case OtpLatchState.EnteringCode:
                    lblOtpLatch.Text = "Entering Code";
                    break;
                case OtpLatchState.WaitingForCodeProcessing:
                    lblOtpLatch.Text = "Waiting";
                    break;
            }

            if (m_otpDetect != null && m_otpLatch == OtpLatchState.EnteringCode)
            {
                if (m_kbHook == null)
                {
                    m_kbHook = new KeyboardHook();
                    m_kbHook.KeyPressed += m_kbHook_KeyPressed;
                }
            }
            else
            {
                if (m_kbHook != null)
                {
                    m_kbHook.Dispose();
                    m_kbHook = null;
                }
            }
        }

        private System.Diagnostics.Process m_wildStarProcess = null;

        /// <summary>
        /// Process a frame.  This is the "main processing" of this program.
        /// </summary>
        /// <returns>Older versions of the prototype would automatically type in your
        /// authenticator code, which takes some time (move the mouse, click the mouse button down,
        /// lift the mouse button up, then repeat for the other five digits).  We don't want to
        /// process additional frames until it's done typing in the code, so this method
        /// returns a Task representing the button clicking process.</returns>
        private async Task DoIt()
        {
            if (m_wildStarProcess == null || m_wildStarProcess.HasExited)
            {
                if (m_wildStarProcess != null)
                    m_wildStarProcess.Dispose();

                // Find WildStar's process.  64-bit only.  32-bit is probably similar, I just didn't
                // bother writing code for it.
                var ps = System.Diagnostics.Process.GetProcessesByName("WildStar64");
                if (ps.Length == 0)
                {
                    this.AppStatus = null;
                    // We can stop right here if we didn't find WildStar running.
                    return;
                }

                m_wildStarProcess = ps[0];
                for (int i = 1; i < ps.Length; ++i)
                    ps[i].Dispose();

                // Let us know when WildStar shuts down so we can drop our reference to the process.  (Should also
                // cleanup DirectX shared surface references and such here too, but this prototype does not.)
                m_wildStarProcess.Exited += delegate { m_wildStarProcess.Dispose(); m_wildStarProcess = null; };
                m_wildStarProcess.EnableRaisingEvents = true;
                this.AppStatus = m_wildStarProcess.Id;
            }

            // Next, check all top-level windows in the system and find the one owned by the WildStar
            // process that has a title that matches the observed title on the WildStar window.
            // I developed against a windowed-fullscreen client.  Dunno what might need to change for
            // a windowed-windowed client.  Probably significantly different for a truly-fullscreen client
            // (especially when it comes to capturing its display).
            var p = m_wildStarProcess;
            IntPtr wshWnd = IntPtr.Zero;
            EnumWindows((hWnd) =>
                {
                    uint pid;
                    var x = NativeMethods.GetWindowThreadProcessId(hWnd, out pid);
                    if (pid == p.Id)
                    {
                        var size = NativeMethods.GetWindowTextLength(hWnd);
                        if (size++ > 0 && NativeMethods.IsWindowVisible(hWnd))
                        {
                            StringBuilder sb = new StringBuilder(size);
                            NativeMethods.GetWindowText(hWnd, sb, size);

                            var s = sb.ToString();
                            if (Regex.Match(s, @"^WildStar\s+\d+").Success)
                            {
                                wshWnd = hWnd;
                                return false;
                            }
                        }
                    }
                    return true;
                });

            this.WindowStatus = wshWnd;

            if (wshWnd != IntPtr.Zero)
            {
                // So at this point WildStar is running and we found its window.  There's some legacy logic in here
                // to find the window position and such that was used with the GDI and DX9 capture routines, but the
                // DX10 capture routine can ignore them here.

                POINT xxp = new POINT(0, 0);
                RECT xxr = new RECT();
                NativeMethods.ClientToScreen(wshWnd, ref xxp);
                NativeMethods.GetClientRect(wshWnd, out xxr);

                // Grab a screenshot of what WildStar's currently showing.
                using (var bib = CaptureWindowDx10(wshWnd, new Rectangle(xxp.X, xxp.Y, xxr.Width, xxr.Height)))
                {
                    // And process that screenshot.
                    await ProcessBitmap(bib, wshWnd);
                }
            }
        }

        /// <summary>
        /// An abstraction over a couple different bitmap representations.  This exists because I originally
        /// prototyped using actual System.Drawing.Bitmap objects and wanted to start swapping them out
        /// with more efficient representations.
        /// </summary>
        private unsafe interface IBitmap : IDisposable
        {
            /// <summary>
            /// Width of the bitmap in pixels.
            /// </summary>
            int Width { get; }

            /// <summary>
            /// Height of the bitmap in pixels.
            /// </summary>
            int Height { get; }

            /// <summary>
            /// A pointer to the pixel at the top left corner of the bitmap.
            /// </summary>
            byte* Scan0 { get; }

            /// <summary>
            /// How many bytes separate rows of the bitmap's data in memory.
            /// </summary>
            int Stride { get; }

            /// <summary>
            /// Create a System.Windows.Forms.Bitmap object from a subset of this IBitmap.
            /// </summary>
            /// <param name="sourceRect">The subset of the bitmap to capture</param>
            /// <returns>A new System.Windows.Forms.Bitmap object</returns>
            Bitmap CreateBitmap(Rectangle sourceRect);

            /// <summary>
            /// Create an IBitmap from a subset of this IBitmap.
            /// </summary>
            /// <param name="sourceRect">The subset of the bitmap to capture</param>
            /// <returns>A new IBitmap object</returns>
            IBitmap CreateIBitmap(Rectangle sourceRect);
        }

        #region capture using GDI

        // **************************************************
        // The GDI capture routines were from an early version of the prototype and might not work
        // with subsequent code updates.  They also happen to be abysmally slow when Windows's DWM is
        // enabled because they force a synchronous copy of the desktop to be done from the video
        // hardware to system memory.  This causes slight hitching in WildStar's framerate.
        //
        // If DWM is not enabled, then GDI capture is probably fine aside from the fact that it
        // allocates System.Windows.Forms.Bitmap objects per-frame and will thus cause lots of
        // GC activity.  If you want to turn this into real, usable code, you'll probably want
        // to fix that.
        // **************************************************

        private Bitmap m_captureBmp;

        private IBitmap CaptureWindow(Rectangle r)
        {
            if (m_captureBmp == null || m_captureBmp.Width != r.Width || m_captureBmp.Height != r.Height)
            {
                if (m_captureBmp != null)
                    m_captureBmp.Dispose();
                m_captureBmp = new Bitmap(r.Width, r.Height);
            }
            using (Graphics g = Graphics.FromImage(m_captureBmp))
            {
                Stopwatch sw = Stopwatch.StartNew();
                g.CopyFromScreen(new Point(r.X, r.Y), new Point(0, 0),
                    new Size(r.Width, r.Height), CopyPixelOperation.SourceCopy);
                sw.Stop();
                //label2.Text = sw.ElapsedMilliseconds.ToString();
            }
            return new BitmapIBitmap(m_captureBmp, false);
        }

        /// <summary>
        /// Wraps a System.Windows.Forms.Bitmap under the IBitmap abstraction.
        /// </summary>
        private unsafe class BitmapIBitmap : IBitmap
        {
            /// <summary>
            /// Create a new instance of this wrapper.
            /// </summary>
            /// <param name="bmp">The System.Windows.Forms.Bitmap object to wrap</param>
            /// <param name="ownBitmap">Whether this wrapper "owns" the underlying Bitmap object
            /// or not.  If so, it will be disposed when this wrapper is disposed.</param>
            public BitmapIBitmap(Bitmap bmp, bool ownBitmap)
            {
                m_bmp = bmp;
                m_ownedBitmap = ownBitmap;
            }

            /// <summary>
            /// Dispose of the wrapper.  May also dispose of the underlying Bitmap object,
            /// if it's owned by this wrapper.
            /// </summary>
            public void Dispose()
            {
                UnlockIt();
                if (m_ownedBitmap)
                    m_bmp.Dispose();
            }

            private readonly Bitmap m_bmp;
            private readonly bool m_ownedBitmap;
            private BitmapData m_bmpData;

            public int Width { get { return m_bmp.Width; } }

            public int Height { get { return m_bmp.Height; } }

            public byte* Scan0 
            { 
                get 
                { 
                    LockIt();
                    return (byte*)m_bmpData.Scan0; 
                }
            }

            public int Stride 
            { 
                get 
                {
                    LockIt();
                    return m_bmpData.Stride; 
                } 
            }

            private void LockIt()
            {
                if (m_bmpData == null)
                    m_bmpData = m_bmp.LockBits(new Rectangle(0, 0, m_bmp.Width, m_bmp.Height), 
                        ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            }

            private void UnlockIt()
            {
                if (m_bmpData != null)
                {
                    m_bmp.UnlockBits(m_bmpData);
                    m_bmpData = null;
                }
            }

            public Bitmap CreateBitmap(Rectangle sourceRect)
            {
                UnlockIt();
                Bitmap b = new Bitmap(sourceRect.Width, sourceRect.Height);
                using (Graphics g = Graphics.FromImage(b))
                {
                    g.DrawImage(m_bmp, new Rectangle(0, 0, b.Width, b.Height), sourceRect, GraphicsUnit.Pixel);
                }
                return b;
            }


            // There's little reason this couldn't return a ByteArrayIBitmap instead.   Probably would be
            // more efficient.  ByteArrayIBitmap just didn't exist when this method was written and before
            // the GDI capture was abandoned.
            public IBitmap CreateIBitmap(Rectangle sourceRect)
            {
                var b = CreateBitmap(sourceRect);
                return new BitmapIBitmap(b, true);
            }
        }

        #endregion

        #region capture using DX9

        // **************************************************
        // The DX9 capture routines use a documented method of getting a capture of the
        // screen.  This might be a little more efficient when DWM is enabled than using GDI to
        // capture the screen; but in practice it still is slow enough to cause framerate hitches
        // in WildStar.  This code also might not even work at all anymore since I stopped
        // caring about it when I switched over to the DX10 capture code.
        // **************************************************

        private Direct3D m_d3d;
        private Device m_device;
        private Surface m_surface;
        private byte[] m_xbuf;
        private int m_xbufStride;
        private GCHandle m_xbufHandle;
               

        private unsafe IBitmap CaptureWindowDx(Rectangle r)
        {
            if (m_device == null)
            {
                PresentParameters present_params = new PresentParameters();
                present_params.Windowed = true;
                present_params.SwapEffect = SwapEffect.Discard;
                m_d3d = new Direct3D();
                m_device = new Device(m_d3d, m_d3d.Adapters[0].Adapter, DeviceType.Hardware,
                        this.Handle,
                        CreateFlags.SoftwareVertexProcessing, present_params);

                m_surface = Surface.CreateOffscreenPlain(m_device, Screen.PrimaryScreen.Bounds.Width,
                    Screen.PrimaryScreen.Bounds.Height, Format.A8R8G8B8, Pool.Scratch);





            }

            Stopwatch sw = Stopwatch.StartNew();

            //var bbSfc = m_device.GetBackBuffer(0, 0);
            //m_device.StretchRectangle(bbSfc,
            //    new Rectangle(0, 0, bbSfc.Description.Width, bbSfc.Description.Height),
            //    m_surface,
            //    new Rectangle(0, 0, Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height),
            //    TextureFilter.Linear);

            
            m_device.GetFrontBufferData(0, m_surface);

            var dr = m_surface.LockRectangle(LockFlags.ReadOnly | LockFlags.NoDirtyUpdate | LockFlags.NoSystemLock);

            if (m_xbuf == null || m_xbuf.Length < Screen.PrimaryScreen.Bounds.Width * Screen.PrimaryScreen.Bounds.Height * 4)
            {
                if (m_xbufHandle.IsAllocated)
                    m_xbufHandle.Free();
                m_xbuf = new byte[Screen.PrimaryScreen.Bounds.Width * Screen.PrimaryScreen.Bounds.Height * 4];
                m_xbufHandle = GCHandle.Alloc(m_xbuf, GCHandleType.Pinned);
            }
            dr.Data.Read(m_xbuf, 0, m_xbuf.Length);

            m_surface.UnlockRectangle();

            //IntPtr ubuf = Marshal.AllocHGlobal(xbuf.Length);
            //Marshal.Copy(xbuf, 0, ubuf, xbuf.Length);
            byte* ubufPtr = (byte*)m_xbufHandle.AddrOfPinnedObject();

            sw.Stop();
            //label2.Text = sw.ElapsedMilliseconds.ToString();

            int stride = Screen.PrimaryScreen.Bounds.Width * 4;
            return ByteArrayBitmap.Create(ubufPtr + (r.X * 4) + (r.Y * stride),
                stride, r.Width, r.Height,
                null);
        }

        /// <summary>
        /// Wraps an in-memory bitmap under the IBitmap abstraction
        /// </summary>
        private unsafe class ByteArrayBitmap : IBitmap
        {
            private static Queue<ByteArrayBitmap> sm_pool = new Queue<ByteArrayBitmap>();

            /// <summary>
            /// Creates a new ByteArrayBitmap object.  This uses an internal object pool to avoid having to do
            /// memory allocations.
            /// </summary>
            /// <param name="buf">A pointer to the underlying bitmap in memory.</param>
            /// <param name="stride">Bytes between rows of the underlying bitmap in memory.</param>
            /// <param name="width">Width of the bitmap.</param>
            /// <param name="height">Height of the bitmap.</param>
            /// <param name="onDispose">An optional action to callback to when this wrapper is disposed.</param>
            /// <returns></returns>
            public unsafe static ByteArrayBitmap Create(byte* buf, int stride, int width, int height, Action onDispose)
            {
                ByteArrayBitmap bab;
                if (sm_pool.Count > 0)
                    bab = sm_pool.Dequeue();
                else
                    bab = new ByteArrayBitmap();

                bab.Initialize(buf, stride, width, height, onDispose);
                return bab;
            }

            private ByteArrayBitmap()
            {
            }

            private void Initialize(byte* buf, int stride, int width, int height, Action onDispose)
            {
                m_buf = buf;
                m_stride = stride;
                m_width = width;
                m_height = height;
                m_onDispose = onDispose;
            }

            /// <summary>
            /// Dispose of this wrapper and add the wrapper object instance back into the object pool.
            /// </summary>
            public void Dispose()
            {
                if (m_onDispose != null)
                {
                    m_onDispose();
                    m_onDispose = null;
                }

                if (sm_pool.Count < 20)
                    sm_pool.Enqueue(this);
            }

            private byte* m_buf;
            private int m_stride;
            private int m_width;
            private int m_height;
            private Action m_onDispose;

            public int Width
            {
                get { return m_width; }
            }

            public int Height
            {
                get { return m_height; }
            }

            public byte* Scan0
            {
                get { return m_buf; }
            }

            public int Stride
            {
                get { return m_stride; }
            }

            public Bitmap CreateBitmap(Rectangle sourceRect)
            {
                Bitmap bmp = new Bitmap(sourceRect.Width, sourceRect.Height);
                BitmapData bd = bmp.LockBits(
                    new Rectangle(0, 0, sourceRect.Width, sourceRect.Height), 
                    ImageLockMode.WriteOnly,
                    PixelFormat.Format32bppArgb);

                //uint rowWidth = (uint)sourceRect.Width * 4;
                byte* ss0 = this.Scan0 + (this.Stride * sourceRect.Y) + (sourceRect.X * 4);
                byte* ds0 = (byte*)bd.Scan0;
                for (int iy = 0; iy < sourceRect.Height; ++iy)
                {
                    byte* sy0 = ss0 + (this.Stride * iy);
                    byte* dy0 = ds0 + (bd.Stride * iy);

                    byte* srcPx = sy0;
                    RGBA* destPx = (RGBA*)dy0;
                    for (int x = 0; x < sourceRect.Width; ++x)
                    {
                        destPx->A = srcPx[3];
                        destPx->R = srcPx[0];
                        destPx->G = srcPx[1];
                        destPx->B = srcPx[2];

                        srcPx += 4;
                        destPx++;
                    }

                    //memcpy((IntPtr)dy0, (IntPtr)sy0, (UIntPtr)rowWidth);
                }

                bmp.UnlockBits(bd);
                return bmp;
            }

            /// <summary>
            /// Creates a new IBitmap from a subregion of this IBitmap.  It is important for performance that
            /// this method does not copy bitmap data.  Instead, the new wrapper points to the same region
            /// of memory that this parent bitmap points to.  The Scan0 of the new sub-bitmap is just updated
            /// appropriately.
            /// </summary>
            /// <param name="sourceRect">The subrect of this bitmap to capture in the new IBitmap</param>
            /// <returns>A new IBitmap wrapper representing the subregion of this bitmap</returns>
            public unsafe IBitmap CreateIBitmap(Rectangle sourceRect)
            {
                byte* cs0 = this.Scan0 + (this.Stride * sourceRect.Y) + (sourceRect.X * 4);
                return ByteArrayBitmap.Create(cs0, this.Stride, sourceRect.Width, sourceRect.Height, null);
            }
        }

        #endregion

        #region capture using DX10

        // **************************************************
        // Ok, first of all the DX10 capture routines rely upon undocumented DWM functionality.
        // It works on my PC (Windows 7 SP1 64-bit), but might not work on yours.  Windows 8
        // has a Desktop Duplication API that could probably be used in place of this, but I don't
        // run Windows 8 so it's up to you to explore that if you want.
        //
        // The core of the undocumented functionality is a call to DwmGetDxSharedSurface(),
        // a native API exposed by user32.  It ostensibly returns a DirectX shared surface of the
        // front buffer of a window on the desktop.
        //
        // I then take that shared surface and copy it to a non-shared DirectX Texture2D.  This
        // is so I can later do stuff on the non-shared Texture2D without worrying about it
        // blocking DWM's desktop composition.
        // **************************************************

        private SlimDX.Direct3D10_1.Device1 m_dx10Device;
        private SlimDX.Direct3D10.Texture2D m_sharedTex;
        private SlimDX.Direct3D10.Texture2D m_myTex;
        private SlimDX.Direct3D10.Texture2D m_myIdentifyTex;

        private const int IDREGION_LEFT = 43;
        private const int IDREGION_UP = 50;
        private const int IDREGION_WIDTH = 50;
        private const int IDREGION_HEIGHT = 24;

        private byte[] m_idBuf;

        private unsafe IBitmap CaptureWindowDx10(IntPtr wshWnd, Rectangle r)
        {
            // Ok so I do a few things here for performance...

            if (m_dx10Device == null)
            {
                // The DX10 device is lazy-initialized.  I didn't include any code in here for cleanup
                // or re-acquisition of the shared surface for the WildStar window, so this program will
                // crash horribly if it's running and you start, shutdown, then re-start WildStar.  If you
                // want to fix that, here's where you'd do it.
                m_dx10Device = new SlimDX.Direct3D10_1.Device1(SlimDX.Direct3D10.DeviceCreationFlags.None,
                    SlimDX.Direct3D10_1.FeatureLevel.Level_10_1);
                IntPtr sh = new IntPtr(-1);
                long p2 = 0, p5 = 0;
                int p3 = 0, p4 = 0;
                NativeMethods.DwmGetDxSharedSurface(wshWnd, ref sh, ref p2, ref p3, ref p4, ref p5);
                m_sharedTex = m_dx10Device.OpenSharedResource<SlimDX.Direct3D10.Texture2D>(sh);

                // This is my non-shared Texture2D.
                SlimDX.Direct3D10.Texture2DDescription tdd = new SlimDX.Direct3D10.Texture2DDescription();
                tdd.Width = m_sharedTex.Description.Width;
                tdd.Height = m_sharedTex.Description.Height;
                tdd.MipLevels = 1;
                tdd.ArraySize = 1;
                tdd.Format = SlimDX.DXGI.Format.B8G8R8A8_UNorm;
                tdd.SampleDescription = new SlimDX.DXGI.SampleDescription(1, 0);
                tdd.Usage = SlimDX.Direct3D10.ResourceUsage.Staging;
                tdd.BindFlags = SlimDX.Direct3D10.BindFlags.None;
                tdd.CpuAccessFlags = SlimDX.Direct3D10.CpuAccessFlags.Read;
                tdd.OptionFlags = SlimDX.Direct3D10.ResourceOptionFlags.None;
                m_myTex = new SlimDX.Direct3D10.Texture2D(m_dx10Device, tdd);

                // This is another, smaller non-shared Texture2D used to identify whether the screen is
                // the WildStar login UI or not.  This is used because the identification needs to be done
                // every frame; and by having this smaller identification region it minimizes the amount
                // of data that needs to be transferred from video memory to system memory every frame
                SlimDX.Direct3D10.Texture2DDescription tdd2 = new SlimDX.Direct3D10.Texture2DDescription();
                tdd2.Width = IDREGION_WIDTH;
                tdd2.Height = IDREGION_HEIGHT;
                tdd2.MipLevels = 1;
                tdd2.ArraySize = 1;
                tdd2.Format = SlimDX.DXGI.Format.B8G8R8A8_UNorm;
                tdd2.SampleDescription = new SlimDX.DXGI.SampleDescription(1, 0);
                tdd2.Usage = SlimDX.Direct3D10.ResourceUsage.Staging;
                tdd2.BindFlags = SlimDX.Direct3D10.BindFlags.None;
                tdd2.CpuAccessFlags = SlimDX.Direct3D10.CpuAccessFlags.Read;
                tdd2.OptionFlags = SlimDX.Direct3D10.ResourceOptionFlags.None;
                m_myIdentifyTex = new SlimDX.Direct3D10.Texture2D(m_dx10Device, tdd2);
            }

            Stopwatch sw = Stopwatch.StartNew();

            // Step 1: Copy the identification region from the shared surface to the identification Texture2D.

            SlimDX.Direct3D10.ResourceRegion rr = new SlimDX.Direct3D10.ResourceRegion();
            rr.Top = r.Height - IDREGION_UP;
            rr.Left = IDREGION_LEFT;
            rr.Bottom = rr.Top + IDREGION_HEIGHT;
            rr.Right = rr.Left + IDREGION_WIDTH;
            rr.Front = 0;
            rr.Back = 1;
            m_dx10Device.CopySubresourceRegion(m_sharedTex, 0, rr, m_myIdentifyTex, 0, 0, 0, 0);

            // Step 2: pull the identification Texture2D from video memory into system memory

            var idm = m_myIdentifyTex.Map(0, SlimDX.Direct3D10.MapMode.Read, SlimDX.Direct3D10.MapFlags.None);
            if (m_idBuf == null || m_idBuf.Length != idm.Data.Length)
            {
                m_idBuf = new byte[idm.Data.Length];
            }
            var idBufStride = idm.Pitch;
            idm.Data.Read(m_idBuf, 0, m_idBuf.Length);
            m_myIdentifyTex.Unmap(0);

            // Step 3: check if the identification region matches what we expect.  WildStar's login UI always has
            // an "Exit" button in the lower left corner of the screen.  The identification region is inside that
            // button, and we identify whether it looks like the exit button by making sure that the red channel
            // of all pixels within that region is greater than the green and blue channels; and also that the
            // green and blue channels aren't above a certain low threshold.

            if (m_xbuf == null || CheckIdentificationRegion(m_idBuf, idBufStride))
            {
                // If we're in here, the identification region checks out, so we'll do the full frame analysis
                this.LoginUiDetect = true;

                // Step 4: copy the entire window from the shared surface to our non-shared Texture2D

                m_dx10Device.CopyResource(m_sharedTex, m_myTex);

                // Step 5: copy our non-shared Texture2D of the entire window from video memory to system memory
                // On my system this takes about 6-8ms; but -- most importantly -- it does not cause any hitching
                // in either DWM or the WildStar rendering.

                var dr = m_myTex.Map(0, SlimDX.Direct3D10.MapMode.Read, SlimDX.Direct3D10.MapFlags.None);

                if (m_xbuf == null || m_xbuf.Length < dr.Data.Length)
                {
                    if (m_xbufHandle.IsAllocated)
                        m_xbufHandle.Free();
                    m_xbuf = new byte[dr.Data.Length];
                    // This buffer needs to be pinned because we're going to be doing direct pointer access into it
                    m_xbufHandle = GCHandle.Alloc(m_xbuf, GCHandleType.Pinned);
                }
                m_xbufStride = dr.Pitch;
                dr.Data.Read(m_xbuf, 0, m_xbuf.Length);

                m_myTex.Unmap(0);
            }
            else
            {
                // If we're in here, it means the identification region didn't check out, so we must not be on the
                // WildStar login UI, so we'll just return an empty bitmap.
                this.LoginUiDetect = false;
                if (m_xbuf[0] != 0 || m_xbuf[1] != 0 || m_xbuf[2] != 0 || m_xbuf[3] != 0)
                {
                    for (int xx = 0; xx < m_xbuf.Length; ++xx)
                    {
                        m_xbuf[xx] = 0;
                    }
                }
            }
            sw.Stop();

            byte* ubufPtr = (byte*)m_xbufHandle.AddrOfPinnedObject();

            int stride = m_xbufStride;
            return ByteArrayBitmap.Create(ubufPtr + (r.X * 4) + (r.Y * stride),
                stride, r.Width, r.Height,
                null);
        }

        private unsafe bool CheckIdentificationRegion(byte[] idbuf, int stride)
        {
            // As mentioned above, our goal here is to check all the pixels in this bitmap to
            // make sure they look like what we expect the inside of the login UI's "Exit" button
            // to look like.  All pixels must have a red channel higher than the green and blue
            // channels; and the green and blue channels must be below a certain threshold.

            fixed (byte* idBufPtr = idbuf)
            {
                for (int y = 0; y < IDREGION_HEIGHT; ++y)
                {
                    byte* curPx = idBufPtr + (stride * y);
                    
                    for (int x = 0; x < IDREGION_WIDTH; ++x)
                    {
                        byte a = curPx[3];
                        byte r = curPx[2];
                        byte g = curPx[1];
                        byte b = curPx[0];

                        if (r < g || r < b || g > 20 || b > 20)
                            return false;

                        curPx += 4;
                    }
                }
            }
            return true;
        }

        #endregion

        /// <summary>
        /// Turn the Win32 EnumWindows() API into something a little more C# idiomatic.
        /// </summary>
        /// <param name="callback">A callback to invoke for each top level window.  Return true
        /// to continue enumeration, or return false to stop enumeration.</param>
        private void EnumWindows(Func<IntPtr, bool> callback)
        {
            NativeMethods.EnumWindows((hWnd, _) => callback(hWnd), IntPtr.Zero);
        }

        /// <summary>
        /// This is the "main entry point" for frame processing.  This is called by a timer component
        /// sitting on the form.
        /// </summary>
        /// <param name="sender">Ostensibly the timer component, but we don't really care.</param>
        /// <param name="e">Irrelevant nonsense.</param>
        private async void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            await DoIt();
            timer1.Enabled = true;
        }

    }
}
