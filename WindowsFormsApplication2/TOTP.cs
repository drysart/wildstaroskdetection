﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication4
{
    // See: http://tools.ietf.org/html/rfc6238

    public static class TOTP
    {
        private static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0);

        public static string GenerateTOTP(byte[] key, DateTime time, int returnDigits)
        {
            return GenerateTOTP(key, time, returnDigits, "HmacSHA1");
        }

        private static int GetHexVal(char hex)
        {
            int val = (int)hex;
            return val - (val < 58 ? 48 : (val < 97 ? 55 : 87));
        }

        private static byte[] HexStringToBytes(string hex)
        {
            if (hex.Length % 2 == 1)
                throw new Exception("Hex string must have an even number of digits");

            byte[] arr = new byte[hex.Length >> 1];
            for (int i = 0; i < hex.Length >> 1; ++i)
            {
                arr[i] = (byte)((GetHexVal(hex[i << 1]) << 4) + (GetHexVal(hex[(i << 1) + 1])));
            }

            return arr;
        }

        private static byte[] HmacSha(string crypto, byte[] keyBytes, byte[] text)
        {
            var hmac = System.Security.Cryptography.HMAC.Create(crypto);
            hmac.Key = keyBytes;
            hmac.Initialize();
            return hmac.ComputeHash(text);
            //return hmac.TransformFinalBlock(text, 0, text.Length);
        }

        private static readonly int[] DigitsPower = new int[]
           // 0  1   2    3     4      5       6        7         8
            { 1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000 };

        public static string GenerateTOTP(byte[] key, DateTime time, int returnDigits, string crypto)
        {
            int codeDigits = returnDigits;
            string result = null;

            long timeStr = Convert.ToInt32(Math.Round((time.ToUniversalTime() - Epoch).TotalSeconds)) / 30;
            byte[] codeIntervalArray = BitConverter.GetBytes(timeStr);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(codeIntervalArray);

            byte[] msg = codeIntervalArray;
            byte[] hash = HmacSha(crypto, key, msg);

            int offset = hash[hash.Length - 1] & 0xf;

            byte[] bytes = new byte[4];
            Array.Copy(hash, offset, bytes, 0, 4);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(bytes);

            uint fullCode = BitConverter.ToUInt32(bytes, 0) & 0x7fffffff;
            uint codeMask = (uint)Math.Pow(10, codeDigits);
            string format = new String('0', codeDigits);
            string code = (fullCode % codeMask).ToString(format);
            return code;
        }
    }
}
