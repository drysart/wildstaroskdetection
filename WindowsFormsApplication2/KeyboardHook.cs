﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication2.Win32Interop;

namespace WindowsFormsApplication2
{
    internal class KeyboardHook : IDisposable
    {
        public KeyboardHook()
        {
            m_hookProc = HookProc;
            m_hookHandle = NativeMethods.SetWindowsHookEx(WH_KEYBOARD_LL, m_hookProc, IntPtr.Zero, 0);
        }

        ~KeyboardHook()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (m_hookHandle != IntPtr.Zero)
            {
                NativeMethods.UnhookWindowsHookEx(m_hookHandle);
                m_hookHandle = IntPtr.Zero;
            }
        }

        private const int WH_KEYBOARD_LL = 13;
        private const int WM_KEYDOWN = 0x0100;

        private IntPtr m_hookHandle;
        private NativeMethods.HookProc m_hookProc;

        public bool Paused { get; set; }

        private unsafe IntPtr HookProc(int nCode, IntPtr wParam, IntPtr lParam)
        {
            KeyboardHookKey? key = null;
            if (wParam.ToInt32() == WM_KEYDOWN && !this.Paused)
            {
                KBDLLHOOKSTRUCT* khs = (KBDLLHOOKSTRUCT*)lParam;

                int vkCode = khs->vkCode;
                if (vkCode >= (int)System.Windows.Forms.Keys.D0 &&
                    vkCode <= (int)System.Windows.Forms.Keys.D9)
                {
                    int digit = vkCode - (int)System.Windows.Forms.Keys.D0;
                    key = (KeyboardHookKey)digit;
                }
                else if (vkCode == (int)System.Windows.Forms.Keys.Back)
                {
                    key = KeyboardHookKey.Backspace;
                }
                else if (vkCode == (int)System.Windows.Forms.Keys.Enter)
                {
                    key = KeyboardHookKey.Enter;
                }
                else if (vkCode == (int)System.Windows.Forms.Keys.Escape)
                {
                    key = KeyboardHookKey.Escape;
                }
            }
            if (key != null)
            {
                OnKeyPressed(key.Value);
                return (IntPtr)1;
            }
            else
            {
                return NativeMethods.CallNextHookEx(m_hookHandle, nCode, wParam, lParam);
            }
        }

        private void OnKeyPressed(KeyboardHookKey value)
        {
            var temp = KeyPressed;
            if (temp != null)
            {
                temp(this, new KeyboardHookEventArgs(value));
            }
        }

        public event EventHandler<KeyboardHookEventArgs> KeyPressed;
    }

    internal class KeyboardHookEventArgs : EventArgs
    {
        public KeyboardHookEventArgs(KeyboardHookKey key)
        {
            this.Key = key;
        }

        public KeyboardHookKey Key { get; private set; }
    }

    internal enum KeyboardHookKey : int
    {
        K0 = 0,
        K1 = 1,
        K2 = 2,
        K3 = 3,
        K4 = 4,
        K5 = 5,
        K6 = 6,
        K7 = 7,
        K8 = 8,
        K9 = 9,

        Backspace = 10,
        Enter = 11,
        Escape = 12
    }
}
