﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication2.Win32Interop
{
    internal enum MouseEventDataXButtons : uint
    {
        None = 0,
        XBUTTON1 = 0x00000001,
        XBUTTON2 = 0x00000002
    }
}
