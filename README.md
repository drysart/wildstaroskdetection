# README #

Because Carbine is apparently a bunch of dummies that militantly don't understand security, I'm dumping this prototype code out there that automatically detects and parses their On-Screen Keyboard and has vestiges of automatic code entry and keystrokes-to-OSK translation.  This is a later version of the code I showed off [on YouTube](https://www.youtube.com/watch?v=ipMNdoYPrWs).

If you want to use this code, expect to put some work into it.  It's a quick-and-dirty prototype that I stopped working on entirely.  I mean, Carbine literally screwed up **almost every single aspect** of their login process:

* They validate your password before they prompt for your OTP; meaning attackers can check your password before they even bother acquiring an authenticator code to use.

* Their one-time passwords aren't **one-time** passwords.  If you're quick, you can log into WildStar, log out, and log back in with **the same 6-digit code**.  [RFC 6238](http://tools.ietf.org/html/rfc6238), the definition of the system that the authenticator implements, even *specifically says not to do that* (near the top of page 7).

* They don't require OTP when logging in to account maintenance on the website.

* They only require a single OTP code to remove an authenticator from your account!

(Those last three points combined mean that an attacker, after passively capturing an authenticator code from you, can remove the authenticator from your account, and then go ahead and add a new one -- locking you out of your account until Carbine's legendary fast support can get to your ticket.)

I mean, while I could have refined this code into something to make the **completely pointless** OSK less painful, but I can't fix the OTP reuse problem.  I can't make their website require 2FA.  I can't make their account maintenance tool require two successive codes to remove an authenticator.  I'm limited in how much of Carbine's incompetence I can spackle over.

And given that they basically struck out on every point they possibly could in creating a simple login process; I decided I don't trust them to be able to securely handle my billing information and cancelled my subscription.  As a result, I have no motivation to turn this prototype into something usable.

So here it is in all its interrupted-work-in-progress glory.  Take it and turn it into something that automatically enters your OTP if you want.  Take it and turn it into something that translates keystrokes into OSK clicks if you want.  Hell, take it and turn it into malware to steal accounts for all I care.

I'm not supporting this code; unless you think you can persuade me otherwise, in which case you can reach me via gmail, [on reddit](http://www.reddit.com/user/drysart), or on the [SA Forums](http://forums.somethingawful.com/member.php?action=getinfo&userid=20065).